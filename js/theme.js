(function($) {

    $(document).ready(function() {

    	// your code here
    	// Carousel
        if($().owlCarousel){

            var wrappCarousel = $(".wrap-carousel"),
            	singleWrapCarousel = $('.wrap-single-carousel');
                
                // console.log(colCarousel);
                wrappCarousel.each(function(index, element){
                    var $this = $(this),
                        colCarousel = $this.find('.owl-carousel'),
                        carouselNav = $this.find('.carousel-nav');

                    var autoscroll = colCarousel.attr("data-autoscroll"); 
                    if(autoscroll == 1) {autoscroll = true;} else {autoscroll = false;}

                    var itemToShow = ($this.data('show')) ? $this.data('show') : 3 ;

                    colCarousel.owlCarousel({
                        autoPlay: autoscroll,
                        items : itemToShow, 
                        margin:10,
                        responsiveClass:true,
                        responsive:{
                            0:{
                                items:1,
                                nav:true
                            },
                            600:{
                                items:3,
                                nav:false,
                            },
                            1000:{
                                items:5,
                                nav:true,
                                loop:false,
                            }
                        },
                        autoWidth : 'true',
                        afterAction : afterAction,
                        pagination : false
                    });

                    
                    function afterAction(){
                        var $this = $(this);

                        maxItem = this.owl.owlItems.length;

                        if(maxItem <= itemToShow ){
                            carouselNav.fadeOut();
                        }else{
                            carouselNav.fadeIn();
                        }
                    }
                          

                    var elPrevNavigation = $this.find('a.carousel-prev'),
                        elNextNavigation = $this.find('a.carousel-next');

                    elNextNavigation.click(function(e){
                        e.preventDefault();
                        colCarousel.trigger('owl.next');
                    });

                    elPrevNavigation.click(function(e){
                        e.preventDefault();
                        colCarousel.trigger('owl.prev');
                    });


                    // console.log(elNavigation);

                });

				// Single Carousel
                if(singleWrapCarousel.length){

                    singleWrapCarousel.each(function(index, element){
                        var $this = $(this),
                            colSingleCarousel = $this.find('.owl-carousel');

                        var autoscroll = colSingleCarousel.attr("data-autoscroll"); 
                        if(autoscroll == 1) {autoscroll = true;} else {autoscroll = false;}

                        colSingleCarousel.owlCarousel({
                            autoPlay: autoscroll,
                            slideSpeed : 300,
                            paginationSpeed : 400,
                            singleItem:true,
                            margin:40,
                            autoWidth : 'true',
                            responsiveClass:true,
                            pagination: false
                        });     

                        var elPrevNavigation = $this.find('a.carousel-prev'),
                            elNextNavigation = $this.find('a.carousel-next');

                        elNextNavigation.click(function(e){
                            e.preventDefault();
                            colSingleCarousel.trigger('owl.next');
                        });

                        elPrevNavigation.click(function(e){
                            e.preventDefault();
                            colSingleCarousel.trigger('owl.prev');
                        });

                    });

                    
                }
      
        }

        if($('#col-vertical').length){

        	var windowDoc = $(document),
        		colVertical = $('#col-vertical'),
        		navLogoBlock = colVertical.find('.nav-logo-block'),
        		shareBlock = colVertical.find('.share-blocks'),
        		spaceBlock = colVertical.find('.space-blocks'),
        		windowDocH = windowDoc.height(),
        		colVerticalHwithoutspace = colVertical.height() - spaceBlock.height();

        		console.log(windowDocH);
        		console.log(colVerticalHwithoutspace);

        	if(colVerticalHwithoutspace < windowDocH){
        		var setHeight = (windowDocH - colVerticalHwithoutspace);

        		spaceBlock.css({"min-height" : setHeight});
        	}


        }

         // responsive tables
        if($().rtResponsiveTables){
            $("#invoice-order").rtResponsiveTables();
        }

        // tile
        if($('.block-tiles').length){

            var tile = $('.tile');

            if($().matchHeight){
                // Match height
                $('.tile').matchHeight();
            }

            setTimeout(function(){
                tile.each(function(index, el) {
                    var imgwrapper = $(this).find('.img-wrapper'),
                        caption = $(this).find('.caption-white'),
                        imgwrapperH = imgwrapper.height(),
                        captionH = caption.height(),
                        imgwrapperW = imgwrapper.width();

                    if((captionH < imgwrapperH) || (caption.width() < imgwrapperW)){
                        setTimeout(function(){
                            caption.height(imgwrapperH);
                            caption.width(imgwrapperW);
                        }, 700)
                    }
                });
            }, 1000);            

            tile.hover(function() {
                var captgrn = $(this).find('.caption-white'),
                    tileAborder = $(this).find('a');
                /* Stuff to do when the mouse enters the element */
                captgrn.css('display', 'block');
                if (Modernizr.csstransitions) {
                    captgrn.addClass('animated');

                    captgrn.removeClass('flipOutX');
                    captgrn.addClass('fadeInLeft');
                } else {
                    captgrn.fadeIn('slow');
                }               

            }, function() {
                /* Stuff to do when the mouse leaves the element */
                var captgrn = $(this).find('.caption-white'),
                    tileAborder = $(this).find('a');

                if (Modernizr.csstransitions) {
                    captgrn.removeClass('fadeInLeft');
                    captgrn.addClass('flipOutX');
                } else {
                    captgrn.fadeOut('fast');
                }
                
            });
        }

        if($().bxSlider){
        	$('.verslider').bxSlider({
			  mode: 'vertical',
			  slideMargin: 5
			});
        }

        if($().nanoScroller){
            $(".nano").nanoScroller({
                alwaysVisible: true
            });
        }

        // select2 bootstrap
        if($().select2){
            $(".select-custom").select2();
        }

        // form-float
         // set active list
        var formFloat = $(".form-float"),
            dropdownMenu = formFloat.parent('.dropdown-menu');

        if(dropdownMenu.length){
            var heightContainer = parseInt(dropdownMenu.outerHeight());// in radians
            console.log(heightContainer)
            $('body').append('<style>.dropdown-menu .form-float:before{font-size: ' + heightContainer + 'px;}</style>');
        }

        if(typeof $JssorSlider$ != "undefined"){
            // Jssor Setup
            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlayInterval: 1500,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                                //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
                $ArrowKeyNavigation: true,                          //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideDuration: 600,                                //Specifies default duration (swipe) for slide in milliseconds

                $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
                    $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
                    $Transitions: [{}],            //[Required] An array of slideshow transitions to play slideshow
                    $TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
                    $ShowLink: true                                    //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
                },

                $ArrowNavigatorOptions: {                       //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 2,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                },

                $ThumbnailNavigatorOptions: {                       //[Optional] Options to specify and enable thumbnail navigator or not
                    $Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always

                    $ActionMode: 1,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                    $Lanes: 2,                                      //[Optional] Specify lanes to arrange thumbnails, default value is 1
                    $SpacingX: 14,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    $SpacingY: 12,                                   //[Optional] Vertical space between each thumbnail in pixel, default value is 0
                    $DisplayPieces: 6,                             //[Optional] Number of pieces to display, default value is 1
                    $ParkingPosition: 0,                          //[Optional] The offset position to park thumbnail
                    $Orientation: 2                                //[Optional] Orientation to arrange thumbnails, 1 horizental, 2 vertical, default value is 1
                }
            };

            var jssor_slider1 = new $JssorSlider$("slidervertical_container", options);
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth)
                    jssor_slider1.$ScaleWidth(Math.max(Math.min(parentWidth, 960), 300));
                else
                    window.setTimeout(ScaleSlider, 30);
            }

            ScaleSlider();

            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                $(window).bind('resize', ScaleSlider);
            }


            //if (navigator.userAgent.match(/(iPhone|iPod|iPad)/)) {
            //    $(window).bind("orientationchange", ScaleSlider);
            //}
            //responsive code end
        }

    });

})(jQuery);